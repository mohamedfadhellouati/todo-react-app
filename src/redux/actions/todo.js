export const actionTypes = {
    ADD_TODO: "ADD_TODO",
    DELETE_TODO: "DELETE_TODO",
    UPDATE_STATE_TODO: "UPDATE_STATE_TODO",
    UPDATE_TODO: "UPDATE_TODO",
    PERSIST_TODOS: "PERSIST_TODOS"
};


export function persistTodos() {
    return dispatch => {
        return dispatch({ type: actionTypes.PERSIST_TODOS });
    };
}

export function addToDo(payload) {
    return dispatch => {
        return dispatch({ type: actionTypes.ADD_TODO,payload });
    };
}

export function deleteToDO(id) {
    return dispatch => {
        return dispatch({ type: actionTypes.DELETE_TODO, id });
    };
}

export function updateStateToDo(id) {
    return dispatch => {
        return dispatch({ type: actionTypes.UPDATE_STATE_TODO, id });
    };
}

export function updateToDo(newTaskValue) {
    return dispatch => {
        return dispatch({ type: actionTypes.UPDATE_TODO, newTaskValue });
    };
}
