import { combineReducers } from "redux";
import { sessionReducer } from 'redux-react-session';
import todo from "./todo";

export default combineReducers({
    session: sessionReducer,
    todo
});
