import { actionTypes } from "redux/actions/todo";

const initialState = {
    todos: []
};

export default function todo(state = initialState, action) {
    switch (action.type) {
        case actionTypes.ADD_TODO:
            return {
                ...state,
                todos: state.todos.concat(action.payload)
            };

        case actionTypes.DELETE_TODO:
            return {
                ...state,
                todos: state.todos.filter((to) => to.id !== action.id)
            };

        case actionTypes.UPDATE_STATE_TODO:
            let newVal = state.todos.map((tod, index) => {
                if (tod.id === action.id) {
                    state.todos[index].status = !state.todos[index].status;
                }
                return state.todos[index]
            })
            return {
                ...state,
                todos: newVal
            };

        case actionTypes.UPDATE_TODO:
            return {
                ...state,
                todos: state.todos.map((tod, index) => {
                    if (tod.id === action.newTaskValue.id) {
                        state.todos[index] = action.newTaskValue;
                    }
                    return state.todos[index]
                })
            };

        default:
            return { ...state };
    }
}