import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import App from 'app/App';
import { sessionService } from 'redux-react-session';
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import reducers from "redux/reducers";
import Loading from 'app/_components/Loading';


let createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
let store = createStoreWithMiddleware(reducers);
sessionService.initSessionService(store);


ReactDOM.render(
  <Provider store={store}>
    <Suspense fallback={<Loading />}>
        <App />
    </Suspense>
  </Provider>,
  document.getElementById('root')
);