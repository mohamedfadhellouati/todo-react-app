import React from "react";
import { withRouter } from "react-router-dom";
import Header from "app/_layouts/Header"
import IndexStyle from './style';


const Home = ({ children }) => {
    const classes = IndexStyle();

    return (
        <>
            <Header />
            <div className={classes.content}>
                {children}
            </div>
        </>
    )
}
export default withRouter(Home);