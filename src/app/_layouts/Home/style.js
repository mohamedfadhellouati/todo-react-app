import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    content: {
        margin: '2rem'
    }
}));

export default IndexStyle;