import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    listItems: {
        flexGrow: 1
    },
    loginButton: {
        padding: "0.2rem 2rem",
        margin: "0rem 1rem"
    },
    title:{
        marginRight:"1.5rem"
    }
}));

export default IndexStyle;