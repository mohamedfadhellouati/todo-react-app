import React, { useState, useEffect } from 'react';

import {
    AppBar,
    Toolbar,
    Grid,
    Divider,
    Button,
    Typography
} from '@material-ui/core';
import NavItem from "app/_components/NavItem"
import { sessionService } from 'redux-react-session';
import IndexStyle from './style';
import {
    Home as HomeIcon
} from 'react-feather';
import { useHistory } from "react-router-dom";

function Header() {
    const classes = IndexStyle();

    let history = useHistory();
    const [userConnected, setUserConnected] = useState(null)


    const items = [
        {
            href: '/',
            icon: HomeIcon,
            title: "Tâches",
            enabled: true
        }
    ];

    const handleLogin = () => {
        sessionService.deleteSession();
        sessionService.deleteUser();
        history.push("/signin");
    }


    useEffect(() => {
        sessionService.loadUser().then(user => {
            if (user !== null) {
                setUserConnected(user)
            }
        }).catch((err) => {
            setUserConnected(null)
        })
    })

    return (
        <>
            <AppBar position="static" color='inherit'>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        TO DO
                    </Typography>

                    <Divider orientation="vertical" flexItem />

                    <div className={classes.listItems}>
                        {items.map((it) => {
                            if (it.enabled) {
                                return <Grid item key={it.title}>
                                    <NavItem
                                        href={it.href}
                                        key={it.title}
                                        title={it.title}
                                        icon={it.icon}
                                    />
                                </Grid>
                            }
                            return null;
                        }
                        )}
                    </div>
                    <Divider orientation="vertical" flexItem />
                    <Button variant="outlined" className={classes.loginButton} onClick={handleLogin}>
                        {userConnected != null ?
                            "Déconnexion"
                            : "Connexion"
                        }
                    </Button>
                </Toolbar>
            </AppBar>
        </>
    );
}
export default Header;