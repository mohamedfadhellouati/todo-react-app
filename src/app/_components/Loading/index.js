import React from 'react'
import {
    Grid,
    Typography,
    CircularProgress
} from '@material-ui/core';
import IndexStyle from './style'


export default function Loading() {
    const classes = IndexStyle();

    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            className={classes.container}
        >
            <Grid item className={classes.circle}>
                <CircularProgress color="secondary" />
            </Grid>
            <Grid item>
                <Typography variant="h5" gutterBottom color="primary">
                    Chargement en cours
            </Typography>
            </Grid>
        </Grid>
    )
}