import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    container: {
        height: "100vh"
    },
    circle: {
        paddingTop: "2%",
        paddingBottom: "2%"
    }
}));

export default IndexStyle;