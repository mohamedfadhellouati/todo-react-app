import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle
} from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import PropTypes from 'prop-types';


const ResponsiveDialog = ({ handleConfirme, setOpen, open, title, contentText}) => {

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));


    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Dialog
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle id="responsive-dialog-title">{title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {contentText}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleConfirme} color="primary">
                    Confirmer
          </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    Annuler
          </Button>
            </DialogActions>
        </Dialog>
    );
}


ResponsiveDialog.propTypes = {
    handleConfirme: PropTypes.func,
    handleClose: PropTypes.func,
    open: PropTypes.bool,
    title: PropTypes.string
};

export default ResponsiveDialog;
