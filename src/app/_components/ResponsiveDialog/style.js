import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    item: {
        display: 'flex',
        paddingTop: 0,
        paddingBottom: 0
    },
    button: {
        color: theme.palette.text.secondary,
        fontWeight: theme.typography.fontWeightMedium,
        justifyContent: 'flex-start',
        letterSpacing: 0,
        padding: "0.7rem 2rem",
        textTransform: 'none',
    },
    icon: {
        marginRight: theme.spacing(1),
        marginLeft: theme.spacing(1)
    },
    title: {
        fontSize:'100%'
    },
    active: {
        color: theme.palette.primary.main,
        '& $title': {
            fontWeight: theme.typography.fontWeightMedium
        },
        '& $icon': {
            color: theme.palette.primary.main
        }
    }
}));

export default IndexStyle;