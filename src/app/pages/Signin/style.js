import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    content: {
        margin: '0 auto',
        width: '50%'
    },
    marginTop: {
        marginTop: "4%"
    },
    inputEmail: {
        maxHeight: "7vh",
        width:"100%"
    },
    fullWidth: {
        width: "100%"
    },
    iconEmail:{
        marginTop: '2vw'
    },
    gridPassword:{
        marginTop: "4vw", 
        marginBottom: "4vw" 
    }
}));

export default IndexStyle;