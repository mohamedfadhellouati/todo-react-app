import React, { useState } from "react";
import IndexStyle from './style';
import { useFormik } from 'formik';
import {
    Button,
    TextField,
    Grid,
    Input,
    FormControl,
    InputLabel,
    IconButton,
    InputAdornment,
    CircularProgress,
    Fade,
    Collapse
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import VpnKeyIcon from '@material-ui/icons/VpnKey';
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import AccountCircle from '@material-ui/icons/AccountCircle';
import CloseIcon from '@material-ui/icons/Close';

import { LoginService } from "services/APIUtils";
import { useHistory } from "react-router-dom";
import { sessionService } from 'redux-react-session';

const Signin = () => {
    const classes = IndexStyle();
    let history = useHistory();

    const [loading, setLoading] = useState(false);
    const [openNotif, setOpenNotif] = useState(false);
    const [alertContent, setAlertContent] = useState(null);
    

    const handleClickShowPassword = (showPassword, setFieldValue) => {
        setFieldValue('showPassword', !showPassword)
    };

    const validate = values => {
        const errors = {};
        const regexEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;


        if (values.password.length > 255) {
            errors.lastName = 'Doit contenir 255 caractères ou moins';
        }
        if (!regexEmail.test(values.email)) {
            errors.email = 'Adresse e-mail invalide';
        }

        if (!values.password) {
            errors.password = 'Obligatoire';
        }
        if (!values.email) {
            errors.email = 'Obligatoire';
        }

        return errors;
    };


    const formik = useFormik({
        initialValues: {
            password: "",
            email: "",
            showPassword: false,
        },
        validateOnChange: false,
        validateOnBlur: false,
        enableReinitialize: true,
        validate,
        onSubmit: values => {
            validate(values)
            const credentials = { email: values.email, password: values.password };
            setLoading((prevLoading) => !prevLoading);
            LoginService(credentials).then(response => {
                if (response.status === 200 ) {
                        sessionService.saveUser(response.data).then(() => {
                            setLoading((prevLoading) => !prevLoading);
                            history.push('/');
                        }).catch(err => console.error(err));
                }
            }).catch(error => {
                setAlertContent(error)
                setLoading((prevLoading) => !prevLoading);
                setOpenNotif(true);
                setTimeout(
                    () => setOpenNotif(false),
                    5000
                );
            });
        },
    });



    return (
        <>
            <div className={classes.content}>
                <Collapse in={openNotif}>
                    <Alert
                        variant="filled" severity="error"
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    setOpenNotif(false);
                                }}
                            >
                                <CloseIcon fontSize="inherit" />
                            </IconButton>
                        }
                    >
                        {alertContent}
                    </Alert>
                </Collapse>

                <form onSubmit={formik.handleSubmit}>
                    <Fade in={true}
                        timeout={500}>
                        <Grid container alignItems="center" spacing={5} className={classes.marginTop}>
                            <Grid item xs={1}>
                                <AccountCircle color={Boolean(formik.errors.email) ? "error" : "inherit"} className={classes.iconEmail} />
                            </Grid>
                            <Grid item xs={10}>
                                <TextField
                                    error={Boolean(formik.errors.email)}
                                    helperText={formik.errors.email}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    value={formik.values.email}
                                    id="email"
                                    name="email"
                                    label="Adresse e-mail *"
                                    fullWidth
                                    size="small"
                                    className={classes.inputEmail} />
                            </Grid>
                        </Grid>
                    </Fade>
                    <Fade in={true}
                        timeout={650}>
                        <Grid container spacing={5} alignItems="flex-end" className={classes.marginTop}>
                            <Grid item xs={1}>
                                <VpnKeyIcon color={Boolean(formik.errors.password) ? "error" : "inherit"} />
                            </Grid>
                            <Grid item xs={10}>
                                <FormControl className={classes.fullWidth}>
                                    <InputLabel htmlFor="password" error={Boolean(formik.errors.password) ? true : false} >Mot de passe *</InputLabel>
                                    <Input
                                        id="password"
                                        error={Boolean(formik.errors.password)}
                                        onBlur={formik.handleBlur}
                                        onChange={formik.handleChange}
                                        type={formik.values.showPassword ? "text" : "password"}
                                        value={formik.values.password}
                                        required
                                        endAdornment={
                                            <InputAdornment>
                                                <IconButton
                                                    onClick={() => handleClickShowPassword(formik.values.showPassword, formik.setFieldValue)}
                                                >
                                                    {formik.values.showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    />
                                    {Boolean(formik.errors.password) && <p class="MuiFormHelperText-root Mui-error MuiFormHelperText-marginDense">{formik.errors.password}</p>}

                                </FormControl>
                            </Grid>
                        </Grid>
                    </Fade>
                    <Grid container
                        justify="center"
                        alignItems="center"
                        className={classes.gridPassword}>

                        <Fade in={loading}
                            style={{
                                display: !loading ? "none" : "block",
                                transformOrigin: "0 0 0"
                            }}
                            timeout={700}>

                            <Grid item xs={2}>
                                <CircularProgress />
                            </Grid>
                        </Fade>

                        <Fade in={!loading}
                            style={{
                                display: loading ? "none" : "block",
                                transformOrigin: "0 0 0"
                            }}
                            timeout={700}>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Connexion
                            </Button>
                        </Fade>
                    </Grid>
                </form>
            </div>
        </>);
};

export default Signin;
