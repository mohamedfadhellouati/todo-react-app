import React, { useState } from "react";
import {
    Typography,
    Fade,
    List,
    ListItem,
    ListItemText,
    Divider,
    Button,
    ListItemSecondaryAction,
    Chip
} from '@material-ui/core';
import IndexStyle from './style';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch, useSelector } from "react-redux";
import { deleteToDO, updateStateToDo } from "redux/actions/todo";
import ResponsiveDialog from 'app/_components/ResponsiveDialog'
import AddTask from 'app/pages/Home/AddTask'


const ListTasks = () => {
    const classes = IndexStyle();
    const dispatch = useDispatch();
    const listTasks = useSelector(state => state.todo.todos);
    const [openDialogConfirm, setOpenDialogConfirm] = useState(false);
    const [taskSelected, setTaskSelected] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false);

    const handleClickOpenDialogConfirm = (id) => {
        setOpenDialogConfirm(true);
        setTaskSelected(id)
    };

    const handleConfirmDialogConfirm = () => {
        dispatch(deleteToDO(taskSelected))
        setTaskSelected(null)
        setOpenDialogConfirm(false);
    };


    const handleClickUpdate = (id) => {
        setIsUpdate(true);
        setTaskSelected(id);
    };



    return (
        <>

            <Fade in={true}
                timeout={600}>
                <div>
                    <Typography variant="h5" noWrap>
                        Liste des tâches
                    </Typography>

                    {listTasks.length > 0 ?

                        <List component="nav" aria-label="contacts">
                            {listTasks.map((task, index) => {
                                return <div key={index + '-container'}>
                                    {isUpdate && task.id === taskSelected ?
                                        <div className={classes.updateForm}>
                                            <AddTask
                                                isupdate={true}
                                                taskSelect={task}
                                                setIsUpdate={setIsUpdate}
                                            />
                                        </div>

                                        :
                                        <ListItem key={index + '-ListItem'}>
                                            <ListItemText
                                                key={index + '-ListItemText'}
                                                primary={task.name}
                                                secondary={
                                                    <React.Fragment key={index + '-info-task'}>
                                                        <Typography
                                                            component="span"
                                                            variant="body2"
                                                            className={classes.inline}
                                                            color="textSecondary"
                                                            key={index + '-name-task'}
                                                        >
                                                            {task.description}
                                                        </Typography>
                                                        {" — "}
                                                        <Button color="primary" key={index + '-update-task'} onClick={() => handleClickUpdate(task.id)}>Modifier</Button>
                                                        {" | "}
                                                        <Button color="primary" key={index + '-delete-task'} onClick={() => handleClickOpenDialogConfirm(task.id)}>Supprimer</Button>
                                                    </React.Fragment>
                                                } />
                                            <ListItemSecondaryAction key={index + '-ListItemSecondaryAction'} onClick={() => dispatch(updateStateToDo(task.id))}>
                                                {task.status ? <Chip icon={<DoneIcon className={classes.iconChip} />} label="Complétée" className={classes.chipBadgeDone} key={index + '-state-task'} /> :
                                                    <Chip icon={<CloseIcon className={classes.iconChip} />} label="Non Complétée" className={classes.chipBadgeNotDone} key={index + '-state-task'} />}
                                            </ListItemSecondaryAction>
                                        </ListItem>
                                    }
                                    <Divider key={index + '-Divider'} />
                                </div>
                            })}
                        </List>

                        :
                        <Typography variant="body2" noWrap color='textSecondary' className={classes.noTasksFound}>
                            Aucune tâche trouvée
                 </Typography>


                    }


                </div>
            </Fade>
            <ResponsiveDialog
                handleConfirme={handleConfirmDialogConfirm}
                setOpen={setOpenDialogConfirm}
                open={openDialogConfirm}
                title='Supprimer la tâche'
                contentText='Voulez-vous supprimer cette tâche ?'
            />
        </>);
};

export default ListTasks;
