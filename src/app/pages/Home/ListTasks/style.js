import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    inline:{
        display: 'inline',
    },
    chipBadgeDone:{
        color:'#fff',
        background:'#00d400',
        cursor:"pointer"
    },
    iconChip:{
        color:'#fff'
    },
    chipBadgeNotDone:{
        color:'#fff',
        background:'red',
        cursor:"pointer"
    },
    updateForm:{
        padding:'1rem'
    },
    noTasksFound:{
        textAlign:'center',
        marginTop:'1rem'
    }
}));

export default IndexStyle;