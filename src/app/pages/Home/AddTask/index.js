import React from "react";
import IndexStyle from './style';
import {
    Typography,
    Fade,
    TextField,
    Button,
    Grid
} from '@material-ui/core';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useFormik } from 'formik';
import { addToDo, updateToDo } from "redux/actions/todo";
import { sessionService } from 'redux-react-session';
import PropTypes from 'prop-types';


const AddTask = ({ isupdate, ...props }) => {
    const classes = IndexStyle();
    const dispatch = useDispatch();
    let history = useHistory();
    const listTasks = useSelector(state => state.todo.todos);

    const validate = values => {
        const errors = {};
        if (!values.name) {
            errors.name = 'Obligatoire';
        }
        if (!values.description) {
            errors.description = 'Obligatoire';
        }

        return errors;
    };

    const handleClickCancelUpdate = () => {
        props.setIsUpdate(false)
    };


    const formik = useFormik({
        initialValues: {
            name: (typeof props.taskSelect !== "undefined" && isupdate ?  props.taskSelect.name : ""),
            description: (typeof props.taskSelect !== "undefined" && isupdate ?  props.taskSelect.description : "")
        },
        validateOnChange: false,
        validateOnBlur: false,
        enableReinitialize: true,
        validate,
        onSubmit: values => {
            sessionService.loadUser().then(user => {
                validate(values)

                if (isupdate) {
                    const taskValue = {
                        id: props.taskSelect.id,
                        name: values.name,
                        description: values.description,
                        idUser: user.id,
                        status: props.taskSelect.status
                    };
                    dispatch(updateToDo(taskValue))
                    props.setIsUpdate(false)
                } else {
                    const taskValue = {
                        id: listTasks.length + 1,
                        name: values.name,
                        description: values.description,
                        idUser: user.id,
                        status: false
                    };
                    dispatch(addToDo(taskValue))
                }
                formik.resetForm();
            }).catch(error => {
                history.push('/signin');
            });

        },
    });

    return (
        <>
            <Fade in={true}
                timeout={600}>
                <form onSubmit={formik.handleSubmit}>
                    {!isupdate &&
                        <Typography variant="h5" noWrap>
                            Créer une nouvelle tâche
                 </Typography>
                    }

                    <Grid
                        container
                        direction="row"
                        justify="flex-start"
                        alignItems="center"
                        spacing={2}
                    >
                        <Grid item className={classes.textFieldContainer}>
                            <TextField
                                id="task-name-id"
                                name="name"
                                error={Boolean(formik.errors.name)}
                                helperText={formik.errors.name}
                                label="Nom de la tâche"
                                className={classes.textField}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange}
                                value={formik.values.name}
                            />

                        </Grid>
                        <Grid item className={classes.textFieldContainer}>
                            <TextField
                                id="task-description-id"
                                name="description"
                                error={Boolean(formik.errors.description)}
                                helperText={formik.errors.description}
                                label="Description de la tâche"
                                className={classes.textField}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange}
                                value={formik.values.description}/>

                        </Grid>
                        <Grid item className={classes.submitContainer}>

                            <Button variant="contained" color="primary" className={classes.button} type="submit">
                                {isupdate ? 'Modifier' :
                                    'Ajouter la tâche'}
                            </Button>

                            {isupdate &&
                                <Button variant="contained" color="secondary" className={classes.button} onClick={handleClickCancelUpdate}>
                                    Annuler
                                </Button>}
                        </Grid>
                    </Grid>
                </form>

            </Fade>

        </>);
};


AddTask.propTypes = {
    isupdate: PropTypes.bool
};

export default AddTask;
