import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    textFieldContainer:{
        width:'35%',
        height: '6rem',
        minWidth: '12rem'
    },
    textField:{
        width:'100%'
    },
    button:{
        marginTop:'1rem',
        marginLeft:'0.5rem'
    },
    submitContainer:{
        height: '6rem'
    }
}));

export default IndexStyle;