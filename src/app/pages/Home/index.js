import React from "react";
import AddTask from "./AddTask";
import ListTasks from "./ListTasks";
import {
    Divider,
    Grid
} from '@material-ui/core';
import IndexStyle from './style';



const Home = () => {
    const classes = IndexStyle();

    return (
        <>
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
                spacing={3}
            >
                <Grid item className={classes.fullWith}>
                    <AddTask isupdate={false}/>
                </Grid>
                <Grid item className={classes.fullWith}>
                    <Divider />
                </Grid>
                <Grid item className={classes.fullWith}>
                    <ListTasks />
                </Grid>
            </Grid>

        </>);
};

export default Home;
