import { makeStyles } from '@material-ui/core/styles';


const IndexStyle = makeStyles((theme) => ({
    imageNotFound: {
        margin: "auto",
        display: "block"
    },
    text:{
        textAlign:'center'
    }
}));

export default IndexStyle;