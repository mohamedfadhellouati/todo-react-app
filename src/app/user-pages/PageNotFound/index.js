import React from "react";
import {
    Typography,
    Fade
} from '@material-ui/core';
import NotFountdLogo from 'resources/images/404-page-not-found.png';
import IndexStyle from './style';

const PageNotFound = () => {
    const classes = IndexStyle();

    return (
        <>
            <Fade in={true}
                timeout={500}>
                <Typography variant="h3" gutterBottom className={classes.text}>
                    Oups !
      </Typography>
            </Fade>

            <Fade in={true}
                timeout={700}>

                <Typography variant="h5" gutterBottom className={classes.text}>
                    La page que vous recherchez semble introuvable
      </Typography>
            </Fade>

            <Fade in={true}
                timeout={1000}>

                <img src={NotFountdLogo} className={classes.imageNotFound} alt="404 not found" />
            </Fade>
        </>
    );

}

export default PageNotFound;
