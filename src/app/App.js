import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Loading from 'app/_components/Loading';
import Layout from "app/_layouts/Home";
import { lazy } from 'react';


const Home = lazy(() => import('app/pages/Home'))
const Signin = lazy(() => import('app/pages/Signin'))
const PageNotFound = lazy(() => import('app/user-pages/PageNotFound'))

const App = () => {

  return <Router>
    <Suspense fallback={<Loading />}>
      <Layout>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/signin">
            <Signin />
          </Route>
          <Route path="*">
            <PageNotFound />
          </Route>
        </Switch>
      </Layout>
    </Suspense>
  </Router>
};

export default App;
